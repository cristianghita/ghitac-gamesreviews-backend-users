'use strict'

const request = require('supertest')
const server = require('../index')
const status = require('http-status-codes')
jest.mock('../modules/database/firebase')

const getUserIndex = {
	uid: 'getuseridindex',
	username: 'getuserindex',
	email: 'get.user.index@gmail.com'
}

const routerPrefix = '/api/v1.0/users'

beforeAll( async() => console.log('Jest starting!'))

//close the server after each test
afterAll(() => {
	server.close()
	console.log('server closed')
})

describe('GET /users/:uid', () => {

	test('retrieve single user', async done => {
		await request(server).post(`${routerPrefix}`)
			.send(getUserIndex)
			.set('Accept', 'application/json')
			.expect(status.CREATED)

		await request(server).get(`${routerPrefix}/byUid/${getUserIndex.uid}`)
			.expect(status.OK)
			.expect( res => {
				res.body.status = 'success'
				res.body.message.user.uid = getUserIndex.uid
				res.body.message.user.username = getUserIndex.username
				res.body.message.user.email = getUserIndex.email
			})

		//delete the freshly created user
		await request(server).del(`${routerPrefix}/byUid/${getUserIndex.uid}`)
			.set('Accept', 'application/json')
			.expect(status.OK)
		done()
	})

	test('retrieve a user that does not exist', async done => {
		await request(server).get(`${routerPrefix}/byUid/999999999`)
			.expect(status.NOT_FOUND)
			.expect( res => {
				res.body.status = 'error'
				done()
			})
	})

	test('trigger the error', async done => {
		await request(server).get(`${routerPrefix}/byUid/999999999`)
			.set('error', 'foo')
			.expect(status.NOT_FOUND)
		done()
	})

})

describe('POST /users', () => {

	test('creating new user', async done => {
		const postuserindex2 = {uid: '8as7dasda', username: 'ghitac', email: 'ghitac@uni.coventry.ac.uk'}
		await request(server).post(`${routerPrefix}`)
			.send(postuserindex2)
			.set('Accept', 'application/json')
			.expect(status.CREATED)
			.expect( res => {
				res.body.status = 'success'
				res.body.message.user.username = 'ghitac'
				res.body.message.user.email = 'ghitac@uni.coventry.ac.uk'
			})
		//delete the freshly created user
		await request(server).del(`${routerPrefix}/byUid/${postuserindex2.uid}`)
			.set('Accept', 'application/json')
			.expect(status.OK)
		done()
	})

	test('creating new user ', async done => { //with username not available
		const postuserindex = {uid: 'postuserindexid', username: 'postuserindex', email: 'post.user.index@gmail.com'}
		await request(server).post(`${routerPrefix}`)
			.send(postuserindex)
			.set('Accept', 'application/json')
			.expect(status.CREATED)
			.expect( res => {
				res.body.status = 'success'
				res.body.message.user.username = postuserindex.username
				res.body.message.user.uid = postuserindex.uid
				res.body.message.user.email = postuserindex.email
			})
		await request(server).post(`${routerPrefix}`)
			.send(postuserindex)
			.set('Accept', 'application/json')
			.expect(status.BAD_REQUEST)
			.expect( res => {
				res.body.status = 'error'
				res.body.message = 'username not available'
			})
		//delete the freshly created user
		await request(server).del(`${routerPrefix}/byUid/${postuserindex.uid}`)
			.set('Accept', 'application/json')
			.expect(status.OK)
		done()
	})

	test('trigger the error', async done => {
		await request(server).post(`${routerPrefix}`)
			.set('error', 'foo')
			.expect(status.BAD_REQUEST)
		done()
	})

})

describe('GET /isUsernameAvailable/:username', () => {
	test('valid username', async done => {
		await request(server).get(`${routerPrefix}/isUsernameAvailable/jdoe244`)
			.set('Accept', 'application/json')
			.expect(status.OK)
			.expect(res => {
				res.body.status = 'success'
				res.body.message.available = true
			})
		done()
	})

	test('invalid username', async done => {
		const invalidUser = {uid: 'invaliduserid', username: 'invaliduser', email: 'invalid.user@gmail.com'}

		await request(server).post(`${routerPrefix}`)
			.send(invalidUser)
			.set('Accept', 'application/json')
			.expect(status.CREATED)

		await request(server).get(`${routerPrefix}/isUsernameAvailable/${invalidUser.username}`)
			.set('Accept', 'application/json')
			.expect(status.OK)
			.expect(res => {
				res.body.status = 'success'
				res.body.message.available = false
			})

		done()
	})

	test('trigger the error', async done => {
		await request(server).get(`${routerPrefix}/isUsernameAvailable/jdoe255`)
			.set('error', 'foo')
			.expect(status.NOT_FOUND)
		done()
	})
})

describe('DELETE /users/:uid', () => {

	test('adding then deleting a single user', async done => {
		const deleteUser = {uid: 'deleteuserindex', username: 'deleteuserindex', email: 'delete.user.index'}
		await request(server).post(`${routerPrefix}`)
			.send(deleteUser)
			.set('Accept', 'application/json')
			.expect(status.CREATED)
		await request(server).del(`${routerPrefix}/byUid/${deleteUser.uid}`)
			.set('Accept', 'application/json')
			.expect(status.OK)
		done()
	})

	test('deleting an item that does not exist should throw an error', async done => {
		await request(server).del(`${routerPrefix}/byUid/999999999`)
			.set('Accept', 'application/json')
			.expect(status.NOT_FOUND)
		done()
	})

	test('trigger the error', async done => {
		await request(server).del(`${routerPrefix}/byUid/999999999`)
			.set('error', 'foo')
			.expect(status.NOT_FOUND)
		done()
	})

})
