'use strict'

const userProcessor = require('../modules/processors/userProcessor')
jest.mock('../modules/database/firebase')

const createUser = {
	uid: 'createuserid',
	username: 'createuser',
	email: 'create.user@gmail.com'
}
const getUser = {
	uid: 'getuserid',
	username: 'getuser',
	email: 'get.user@gmail.com'
}
const availableUsername = 'availableUsername'
const existingUser = {
	uid: 'existinguserid',
	username: 'existinguser',
	email: 'existing.user@gmail.com'
}
const deleteUser = {
	uid: 'deleteuserid',
	username: 'deleteuser',
	email: 'delete.user@gmail.com'
}
const deleteUser2 = {
	uid: 'deleteuserid2',
	username: 'deleteuser2',
	email: 'delete.user2@gmail.com'
}

describe('doCreateUser', () => {
	test('creating new user', async done => {
		const request = {body: createUser}
		const data = await userProcessor.doCreateUser(request)
		expect(data.username).toBe(createUser.username)
		await userProcessor.doDeleteUserByUid(request) //clean new user - maybe not the best place for it
		done()
	})
})

describe('getUserByUid', () => {
	test('retrieve user that was just created', async done => {
		const request = {body: getUser}
		await userProcessor.doCreateUser(request)
		const result = await userProcessor.getUserByUid(request)
		expect(result.email).toBe(getUser.email)
		expect(result.username).toBe(getUser.username)
		expect(result.uid).toBe(getUser.uid)
		await userProcessor.doDeleteUserByUid(request) //clean new user - maybe not the best place for it
		done()
	})

	test('retrieve user that does not exist', async done => {
		try {
			await userProcessor.getUserByUid({body: {uid: '99xa999axa9x9x'}})
		} catch(err) {
			expect(err.message).toBe('user not found')
			done()
		}
	})
})

describe('isUsernameAvailable', () => {
	test('username available', async done => {
		const request = {body: {username: availableUsername}}
		const result = await userProcessor.isUsernameAvailable(request)
		expect(result.available).toBe(true)
		done()
	})

	test('username not available', async done => {
		const request = {body: existingUser}
		await userProcessor.doCreateUser(request)
		const result = await userProcessor.isUsernameAvailable(request)
		expect(result.available).toBe(false)
		await userProcessor.doDeleteUserByUid(request) //clean new user - maybe not the best place for it
		done()
	})
})

describe('doDeleteUserByUid', () => {
	test('delete user', async done => {
		try {
			const request = {body: deleteUser}
			await userProcessor.doCreateUser(request)
			const data = await userProcessor.doDeleteUserByUid(request)
			expect(data.uid).toBe(deleteUser.uid)
			await userProcessor.getUserByUid(request)
		} catch(err) {
			expect(err.message).toBe('user not found')
			done()
		}
	})

	test('delete user that does not exist', async done => {
		try {
			const request = {body: deleteUser2}
			await userProcessor.doDeleteUserByUid(request)
		} catch(err) {
			expect(err.message).toBe('user not found')
			done()
		}
	})
})
