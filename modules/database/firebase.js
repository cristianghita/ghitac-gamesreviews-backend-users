'use strict'

const firebase = require('../../node_modules/firebase')

const config = {
	apiKey: 'AIzaSyBv6gfYrCIIbn-Xq4vWlaI9SRlgybfqksI',
	authDomain: 'gamesreviews-backend.firebaseapp.com',
	databaseURL: 'https://gamesreviews-backend.firebaseio.com',
	projectId: 'gamesreviews-backend',
	storageBucket: 'gamesreviews-backend.appspot.com',
	messagingSenderId: '340179765155'
}

firebase.initializeApp(config)

const db = firebase.database()
module.exports.db = db

module.exports.getUserByUid = async user => {
	const usersRef = db.ref(`users/${user.uid}`)
	const snapshot = await usersRef.once('value')

	let userToReturn

	if(snapshot.exists()) {
		userToReturn = snapshot.val()
		userToReturn.uid = snapshot.key
	} else {
		throw Error('user not found')
	}

	return userToReturn
}

module.exports.doCreateUser = async user => {
	const available = (await this.isUsernameAvailable({username: user.username})).available
	if(!await available) throw Error('username not available')
	await db.ref(`users/${user.uid}`).set({
		username: user.username,
		email: user.email
	})
	return user
}

module.exports.isUsernameAvailable = async user => {
	const usersRef = db.ref('users').orderByChild('username').equalTo(user.username)
	const snapshot = await usersRef.once('value')

	let available

	if(snapshot.exists()) {
		available = false
	} else {
		available = true
	}

	return {available: available}
}

module.exports.doDeleteUserByUid = async user => {
	const usersRef = db.ref(`users/${user.uid}`)
	const snapshot = await usersRef.once('value')

	let userToReturn

	if(snapshot.exists()) {
		userToReturn = snapshot.val()
		userToReturn.uid = snapshot.key
		usersRef.remove()
	} else {
		throw Error('user not found')
	}

	return userToReturn
}
