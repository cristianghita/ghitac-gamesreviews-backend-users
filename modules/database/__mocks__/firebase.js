'use strict'

const users = []

module.exports.getUserByUid = async user => {
	for (const dbuser in users) {
		if(users[dbuser].uid === user.uid) return users[dbuser]
	}

	throw Error('user not found')
}

module.exports.doCreateUser = async user => {
	const available = (await this.isUsernameAvailable(user)).available
	if(!available) throw Error('username not available')
	users.push(user)
	return users[users.length - 1]
}

module.exports.isUsernameAvailable = async user => {
	for (const dbuser in users) {
		if(users[dbuser].username === user.username) return {available: false}
	}

	return {available: true}
}

module.exports.doDeleteUserByUid = async user => {
	for (const dbuser in users) {
		const myUser = users[dbuser]
		if(myUser.uid === user.uid) {
			users.splice(dbuser, 1)
			return myUser
		}
	}

	throw Error('user not found')
}
