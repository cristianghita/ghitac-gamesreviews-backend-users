'use strict'

const db = require('../database/firebase')

module.exports.getUserByUid = async request => {
	const user = await this.extractUserData(request)
	const userToReturn = await db.getUserByUid(user)
	return userToReturn
}

module.exports.doCreateUser = async request => {
	const user = await this.extractUserData(request)
	const userToReturn = await db.doCreateUser(user)
	return userToReturn
}

module.exports.isUsernameAvailable = async request => {
	const user = await this.extractUserData(request)
	const available = await db.isUsernameAvailable(user)
	return available
}

module.exports.doDeleteUserByUid = async request => {
	const user = await this.extractUserData(request)
	const userToReturn = await db.doDeleteUserByUid(user)
	return userToReturn
}

module.exports.extractUserData = request => ({
	uid: request.body.uid,
	username: request.body.username,
	email: request.body.email})
