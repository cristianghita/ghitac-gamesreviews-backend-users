
FROM node:10.13.0
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
RUN npm install -g nodemon
COPY package*.json /usr/src/app/
RUN npm install
COPY . .
EXPOSE $PORT 8080
CMD [ "npm", "start" ]
