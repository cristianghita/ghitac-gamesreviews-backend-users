
'use strict'

const Koa = require('koa')
const bodyParser = require('koa-bodyparser')
const cors = require('@koa/cors')

const app = new Koa()

app.use(cors())
app.use(bodyParser())
const router = require('koa-router')({
	prefix: '/api/v1.0/users'
})

const status = require('http-status-codes')

const userProcessor = require('./modules/processors/userProcessor')

const defaultPort = 8080
const port = process.env.PORT || defaultPort


router.get('/byUid/:uid', async ctx => {
	ctx.set('Allow', 'GET, DELETE')
	try {
		if(ctx.get('error')) throw new Error(ctx.get('error'))
		const user = await userProcessor.getUserByUid({body: {uid: ctx.params.uid}})
		ctx.status = status.OK
		ctx.body = {status: 'success', message: {user: user}}
	} catch(err) {
		ctx.status = status.NOT_FOUND
		ctx.body = {status: 'error', message: err.message}
	}
})

router.get('/isUsernameAvailable/:username', async ctx => {
	ctx.set('Allow', 'GET')
	try {
		if(ctx.get('error')) throw Error(ctx.get('error'))
		const res = await userProcessor.isUsernameAvailable({body: {username: ctx.params.username}})
		ctx.status = status.OK
		ctx.body = {status: 'success', message: {available: res.available}}
	} catch(err) {
		ctx.status = status.NOT_FOUND
		ctx.body = {status: 'error', message: err.message}
	}
})

router.post('/', async ctx => {
	ctx.set('Allow', 'POST')
	try {
		if(ctx.get('error')) throw new Error(ctx.get('error'))
		const user = await userProcessor.doCreateUser(await ctx.request)
		ctx.status = status.CREATED
		ctx.body = {status: 'success', message: {user: user}}
	} catch(err) {
		ctx.status = status.BAD_REQUEST
		ctx.body = {status: 'error', message: err.message}
	}
})

router.del('/byUid/:uid', async ctx => {
	ctx.set('Allow', 'GET, DELETE')
	try {
		if(ctx.get('error')) throw new Error(ctx.get('error'))
		const user = await userProcessor.doDeleteUserByUid({body: {uid: ctx.params.uid}})
		ctx.status = status.OK
		ctx.body = {status: 'success', message: {user: user}}
	} catch(err) {
		ctx.status = status.NOT_FOUND
		ctx.body = {status: 'error', message: err.message}
	}
})

app.use(router.routes())
app.use(router.allowedMethods())
const server = app.listen(port)
console.log(`app running on port [${port}]`)

module.exports = server
